#!/usr/bin/python
import itertools
from operator import mul

N = 0  # Number of cases
I = 0  # Number of Items in each case
x = 0  # used to split the item list
vector1items = []  # array of items for each case
vector2items = []
caseNum = 0  # the specific case Number
minScalarProduct = 0  # minimum product of each scalar (variable)


# open the file for reading
#f=open('A-small-practice.in','r')
#g=open('minimum_scalar_product-small.out', 'w')
f=open('A-large-practice.in','r')
g=open('minimum_scalar_product-large.out', 'w')

g.truncate()

# get number of cases by reading the first line.  convert to integer.
N = int(f.readline())

# loop through the number of cases

# Note this doesnt check every combination, but sorts the lists such that the first is going from smallest to largest and the
# second is going from largest to smallest, effectively giving the smallest results when doing multiplication.
# TODO: plan to come back to this after learning how to do combinations that will meet the requirements:
# (multiple arrays that each have a different combination of the two given)

for caseNum in (range(N)):
    I = f.readline().strip()  # read the next line for the number of items in the case
    vector1itemFullList = f.readline()  # read the next line for a space-separated list of item prices
    vector1items = vector1itemFullList.split(' ')  # split the list by 'space' so one list gets broken up into each item
    vector1items = [x.strip() for x in vector1items]  # remove extra spaces and newline characters (should only be numbers)
    vector1items = map(int, vector1items) # convert strings to integers
    vector1items.sort() # sort list Ascecending


    vector2itemFullList = f.readline()  # read the next line for a space-separated list of item prices
    vector2items = vector2itemFullList.split(' ')  # split the list by 'space' so one list gets broken up into each item
    vector2items = [x.strip() for x in vector2items]  # remove extra spaces and newline characters (should only be numbers)
    vector2items = map(int, vector2items) # convert strings to integers
    vector2items.sort(reverse=True) # sort list Decending

    for i in range(int(I)):  # loop through the number of items
        multiple = (int(vector1items[i])*int(vector2items[i])) # multiply the item slot of each list
        minScalarProduct += multiple # add that number to the a running sum

    caseNumNew = caseNum + 1  # since case starts with 0 due to the array.  adding 1 to compensate
    print "Case #" + str(caseNumNew) + ": " + str(minScalarProduct)
    g.write("Case #" + str(caseNumNew) + ": " + str(minScalarProduct))
    g.write("\n")

    minScalarProduct = 0  # reset the match number to zero for the next case


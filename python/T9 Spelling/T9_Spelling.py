#!/usr/bin/python

#f=open('C-small-practice.in','r')
f=open('C-large-practice.in','r')

C=f.readline().strip()
numbers = ""

def letters_to_numbers(argument):
    list = {
        "a": 2,
        "b": 22,
        "c": 222,
        "d": 3,
        "e": 33,
        "f": 333,
        "g": 4,
        "h": 44,
        "i": 444,
        "j": 5,
        "k": 55,
        "l": 555,
        "m": 6,
        "n": 66,
        "o": 666,
        "p": 7,
        "q": 77,
        "r": 777,
        "s": 7777,
        "t": 8,
        "u": 88,
        "v": 888,
        "w": 9,
        "x": 99,
        "y": 999,
        "z": 9999,
        " ": 0
    }
    return list.get(argument, "nothing")


for x in range(int(C)):
    data=f.readline().rstrip('\n')
    letters = ""
    numbers = ""
    letters = list(data)
    for y in range(len(letters)):
        if (y > 0 and str(letters_to_numbers(letters[y]))[:1] == str(letters_to_numbers(letters[y-1]))[:1]):
            numbers = numbers + " " + str(letters_to_numbers(letters[y]))
        else:
            numbers = numbers + str(letters_to_numbers(letters[y]))
    CaseNum = x+1
    print "Case #" + str(CaseNum) + ": " + numbers
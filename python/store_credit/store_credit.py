#!/usr/bin/python

########################################################################################
##
##	Program Name:	Store Credit
##	Author:		Jeff Kassel
##	Date:		12/9/2015
##	
##	Description: Written to answer a google codejam practice question.
##	URL: https://code.google.com/codejam/contest/351101/dashboard#s=p0  
##
##
##	Problem
##
##	You receive a credit C at a local store and would like to buy two items. 
##	You first walk through the store and create a list L of all available items. 
##	From this list you would like to buy two items that add up to the entire value of 
##	the credit. 
##	The solution you provide will consist of the two integers indicating the positions of 
##	the items in your list (smaller number first).
##	
##
##	Input
##
##	The first line of input gives the number of cases, N. N test cases follow. 
##
##  For each test case there will be:
##	One line containing the value C, the amount of credit you have at the store.
##	One line containing the value I, the number of items in the store.
##	One line containing a space separated list of I integers. Each integer P indicates 
##	the price of an item in the store.
##
##	Each test case will have exactly one solution.
##	
##	Output
##
##	For each test case, output one line containing "Case #x: " followed by the indices 
##	of the two items whose price adds up to the store credit.
##	The lower index should be output first.
##
##	Limits
##
##	5 <= C <= 1000
##	1 <= P <= 1000
##
##	Small dataset
##
##	N = 10
##	3 <= I <= 100
##
##	Large dataset
##
##	N = 50
##	3 <= I <= 2000
##
##	Sample
##	
##
##	Input  
##	3
##	100
##	3
##	5 75 25
##	200
##	7
##	150 24 79 50 88 345 3
##	8
##	8
##	2 1 9 4 4 56 90 3
##
##	Output
##	Case #1: 2 3
##	Case #2: 1 4
##	Case #3: 4 5 
##
########################################################################################

#  PROGRAM BEGINS HERE #

N = 0  # Number of cases
C = 0  # Budget for each case
I = 0  # Number of Items in each case
x = 0  # used to split the item list
items = []  # array of items for each case
caseNum = 0  # the specific case Number
itemCost = 0  # Cost of that specific item
itemMatch1 = 0  # Item1 that adds up to the budget
itemMatch2 = 0  # Item2 that adds up to the budget

# open the file for reading
#f = open('A-small-practice.in', 'r')
f = open('A-large-practice.in', 'r')

# get number of cases by reading the first line.  convert to integer.
N = int(f.readline())

# loop through the number of cases
for caseNum in (range(N)):
    C = f.readline().strip()  # read the next line for the budget of that case
    I = f.readline().strip()  # read the next line for the number of items in the case
    itemFullList = f.readline()  # read the next line for a space-separated list of item prices
    items = itemFullList.split(' ')  # split the item price list by 'space' so one list gets broken up into each item
    items = [x.strip() for x in items]  # remove extra spaces and newline characters (should only be numbers)

    #########################################################################################
    ##	for this next loop, we are going through the list of prices.
    ##
    ##	on each price "slot", we assume this is candidate for itemMatch1.
    ##
    ##	We then use this number and try to add it to each price in the list starting from
    ##	the beginning again.
    ##
    ##	when we reach the candidate, we skip it since we cant use it twice in the same match
    ##	test.
    ##
    ##	if they both add up to the budget cost, C, then we have a match.
    ##
    ##	otherwise we move on to the next price in the item list as a new candidate and re-run
    ##	the test again.
    #########################################################################################

    for i in range(len(items)):  # loop through the number of items
        for j in range(len(items)):  # for each item, loop through all the items again
            if len(items) == 1:  # if there is only 1 item in the list,
                itemMatch1 = items[i]  # then that one item is itemMatch1.
                itemMatch2 = 0  # the other is obviously 0 since it doesnt exist.
                break  # no need to continue this loop, we have our match
            if j == i:  # if the item number is the same in both lists then skip
                continue  # (cant reuse the same number slot)
            if int(items[i]) + int(items[j]) == int(C):  # if the item from the first list and the item from the second
                # list add up to the budget evenly, then we have a match!
                if i+1 > j+1:  # check if the first list item (index, don't forget to add one due to array starting with 0) is larger than the second
                    itemMatch1 = j+1  # set second list item as itemMatch1 (remember smaller index goes first)
                    itemMatch2 = i+1  # set first list item as itemMatch2
                if i+1 < j+1:  # check if the first list item is smaller than the second
                    itemMatch1 = i+1  # set first list item as itemMatch1 (remember smaller index goes first)
                    itemMatch2 = j+1  # set second list item as itemMatch2
                if i+1 == j+1:  # check if both values are the same (should never be the case)
                    itemMatch1 = i+1  # order doesnt matter since they are the same
                    itemMatch2 = j+1  # order doesnt matter since they are the same
                    break  # no need to continue, we have our match.
    # print this case with the budget and matching information
    # (updated from original asked format for easier reading
    # print "Case #",caseNum,"\t Budget: ", C, "\t\t Match 1: ",  itemMatch1, "\t\tMatch 2: ", itemMatch2
    caseNumNew = caseNum + 1  # since case starts with 0 due to the array.  adding 1 to compensate
    print 'Case #' + str(caseNumNew) + ': ' + str(itemMatch1) + ' ' + str(itemMatch2)
    itemMatch1 = 0  # reset the match number to zero for the next case
    itemMatch2 = 0  # reset the match number to zero for the next case

# PROGRAM ENDS HERE #

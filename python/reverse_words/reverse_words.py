#!/usr/bin/python

#f = open('B-small-practice.in', 'r')
f = open('B-large-practice.in', 'r')

C = f.readline().strip()
reversedList = ""
word = ""

for x in range(int(C)):
    words = f.readline().strip().split(' ')
    lastWordIndex = len(words) - 1
    wordIndex = lastWordIndex
    while (wordIndex <= lastWordIndex and wordIndex >= 0):
        word = words[wordIndex]
        reversedList = reversedList + " " + word
        if wordIndex == 0:
            break
        wordIndex -= 1
    CaseNum = x+1
    print "Case #" + str(CaseNum) + ":" + str(reversedList)
    reversedList = ""
